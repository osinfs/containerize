FROM registry.access.redhat.com/rhel7

LABEL maintainer 'Adriano Vieira <adriano.svieira at gmail.com>'

ADD setup/puppet_tests/Gemfile /usr/src/puppet_tests/Gemfile

ENV LANG="en_US.UTF-8"
ENV PUPPET_VERSION='~> 3.8.0'

# Dataprev subscription-manager
#RUN rpm -Uvh http://v241h009.prevnet/pub/katello-ca-consumer-latest.noarch.rpm
#RUN subscription-manager register --org='Default_Organization' --activationkey='dev_rhel_7' --force
#RUN subscription-manager attach --auto

# check pacake names on:
#  https://pkgs.alpinelinux.org/packages
#  http://dl-3.alpinelinux.org/alpine/
# install base packs for puppet tests
RUN yum install -y git gcc make ruby-devel rubygem-bundler --enablerepo=*; \
    bundle install --gemfile=/usr/src/puppet_tests/Gemfile --clean --no-cache --no-prune ; \
    yum clean all; unset https_proxy; unset http_proxy
