FROM alpine
LABEL maintainer 'Adriano Vieira <adriano.svieira at gmail.com>'
# install base packs
RUN apk add --update --no-cache curl && \
    curl -L https://github.com/spf13/hugo/releases/download/v0.17/hugo_0.17_Linux-64bit.tar.gz -o /var/cache/hugo_0.17_Linux-64bit.tar.gz && \
    tar zxvf /var/cache/hugo_0.17_Linux-64bit.tar.gz -C /var/cache && \
    chmod a+x /var/cache/hugo_0.17_linux_amd64/hugo_0.17_linux_amd64 && \
    mv /var/cache/hugo_0.17_linux_amd64/hugo_0.17_linux_amd64 /usr/local/bin/hugo ; \
    apk del --purge curl ; \
    rm -rf /var/cache/hugo_0.17_Linux-64bit.tar.gz /var/cache/hugo_0.17_linux_amd64

# possible environment variables to setup
ENV HUGO_BASE_URL http://localhost
ENV HUGO_BASE_URL_PORT 1313
ENV HUGO_BUILD_DRAFTS --buildDrafts
ENV HUGO_WATCH false

# possible hugo ports
EXPOSE 1313
EXPOSE 80

WORKDIR /var/www/html/site

# add your root source <website> directory
ONBUILD ADD . /var/www/html/site

# run Hugo site
CMD hugo server --verbose --verboseLog --watch=${HUGO_WATCH} ${HUGO_BUILD_DRAFTS} --bind=0.0.0.0 --baseURL ${HUGO_BASE_URL} --port ${HUGO_BASE_URL_PORT}
