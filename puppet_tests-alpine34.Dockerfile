FROM docker:1.12-git

LABEL maintainer 'Adriano Vieira <adriano.svieira at gmail.com>'

ADD setup/puppet_tests/Gemfile /usr/src/puppet_tests/Gemfile

ENV LANG="en_US.UTF-8"

# check pacake names on:
#  https://pkgs.alpinelinux.org/packages
#  http://dl-3.alpinelinux.org/alpine/
# install base packs for puppet tests
RUN apk add --update --no-cache ruby ruby-dev ruby-rake ruby-bundler build-base zlib-dev; \
    bundle install --gemfile=/usr/src/puppet_tests/Gemfile --clean --no-cache --no-prune
