FROM docker:1.12-git

LABEL maintainer 'Adriano Vieira <adriano.svieira at gmail.com>'

ENV LANG="en_US.UTF-8"

# chack pacake names on:
#  https://pkgs.alpinelinux.org/packages
# install base packs for python tests
# python pip pytest
RUN apk add --update --no-cache python py-pip gcc; \
    pip install --upgrade --no-cache-dir pip pytest

# Rancher CLI to manage Rancher Server +1.6
RUN curl -L https://releases.rancher.com/cli/v0.6.10/rancher-linux-amd64-v0.6.10.tar.gz -o /tmp/rancher-0.6.10.tgz && \
    tar zxvf /tmp/rancher-0.6.10.tgz -C /tmp && \
    mv /tmp/rancher-v0.6.10/rancher /usr/local/bin/rancher-0.6.10 && \
    rm -rf /tmp/rancher-v0.6.10 /tmp/rancher-0.6.10.tgz

# Rancher CLI to manage Rancher Server 2.x
RUN curl -L https://releases.rancher.com/cli/v2.0.3/rancher-linux-amd64-v2.0.3.tar.gz -o /tmp/rancher-2.0.3.tgz && \
    tar zxvf /tmp/rancher-2.0.3.tgz -C /tmp && \
    mv /tmp/rancher-v2.0.3/rancher /usr/local/bin/rancher-2.0.3 && \
    rm -rf /tmp/rancher-v2.0.3 /tmp/rancher-2.0.3.tgz
