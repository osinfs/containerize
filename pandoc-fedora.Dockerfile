FROM fedora:28

LABEL maintainer 'Adriano Vieira <adriano.svieira at gmail.com>'

#ENV LANG="en_US.UTF-8"

# install Pandoc
RUN dnf install --nodocs --assumeyes \
                pandoc texlive texlive-flowfram texlive-ly1 texlive-xcharter texlive-wrapfig && \
    dnf clean all
